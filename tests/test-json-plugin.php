<?php

/**
 * Base plugin tests to ensure the JSON API is loaded correctly. These will
 * likely need the most changes when merged into core.
 *
 * @group json_api
 *
 * @package WordPress
 * @subpackage Pixo JSON API
 */
class WP_Test_Pixo_JSON_Plugin extends WP_UnitTestCase {

	/**
	 * The plugin should be installed and activated.
	 */
	function test_plugin_activated() {
		$this->assertTrue( class_exists( 'Pixo_JSON_Menus' ) );
	}

	/**
	 * The json_api_init hook should have been registered with init, and should
	 * have a default priority of 10.
	 */
	function test_wp_json_server_before_serve_action_added() {
		$this->assertEquals( 10, has_action( 'wp_json_server_before_serve', 'pixo_register_api_endpoints' ) );
	}

}
