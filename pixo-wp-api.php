<?php 
/*
Plugin Name: Pixo WP API
Plugin URI: http://www.pixotech.com/
Description: Extend WP API
Author: Copyright (C) 2016 On the Job Consulting Inc. d/b/a Pixo
Version: 1.0
Author URI: http://www.pixotech.com/
*/

require_once __DIR__ . '/lib/class-wp-json-menus.php';
require_once __DIR__ . '/lib/class-wp-json-options.php';

if(!function_exists('pixo_register_api_endpoints')) :
  function pixo_register_api_endpoints() {
    global $Pixo_JSON_Menus;
    global $Pixo_JSON_Options;
    $Pixo_JSON_Menus = new Pixo_JSON_Menus();
    $Pixo_JSON_Options = new Pixo_JSON_Options();
    add_filter( 'json_endpoints', [$Pixo_JSON_Menus, 'register_routes'] );
    add_filter( 'json_endpoints', [$Pixo_JSON_Options, 'register_routes'] );
  }
  add_action( 'wp_json_server_before_serve', 'pixo_register_api_endpoints' );
endif;

// WP API OAuth Support
// add_filter( 'http_request_host_is_external', 'pixo_allow_local_oauth_requests', 10, 3 );