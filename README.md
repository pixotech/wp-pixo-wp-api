This plugin extends the WP JSON API by adding routes for menus, a sitemap, and values for ACF-powered custom options forms.  It's based on V1 of the WP API.  It also has an option to allow local OAuth requests in case that's needed for the API.  

# Menus Routes

## /menus
Gets you all the menus and their menu items.

## /menus/sitemap
Gets you the sitemap.

## /menus/<name>
Gets you a menu's items by menu slug.

## /menus/<mid>
Gets you a menu's items by menu id.

## /menus/all/meta
Gets you metadata about all menus.

## /menus/<name>/meta
Gets you metadata about a single menu, specified by its slug.

## /menus/<mid>/meta
Gets you metadata about a single menu, specified by its menu id.

# ACF Options Routes

## /acf_options
Gets you all the options and values from every custom ACF options form.

## /acf_options/<slug>
Gets you the options from a single custom ACF options form specified by the form's slug.

# Tests
The tests for this plugin are incomplete.

# OAuth
A WP filter must be used to allow local OAuth requests if they'll be coming from the same host, and that filter is setup in this plugin but commented out.
