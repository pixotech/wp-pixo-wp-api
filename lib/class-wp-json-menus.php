<?php 

if( !class_exists('Pixo_JSON_Menus') ) :
	class Pixo_JSON_Menus {

		public function register_routes( $routes ) {
			$menus_routes = [
				'/menus' => [
					[ [ $this, 'get_menus' ],								WP_JSON_Server::READABLE ],
				],
				// Don't put '/menus/sitemap' lower, it breaks.
				'/menus/sitemap' => [
					[ [ $this, 'get_sitemap' ], 						WP_JSON_Server::READABLE ],
				],
				'/menus/(?P<name>[a-z-]+)' => [
					[ [ $this, 'get_menu_items_by_name' ], 	WP_JSON_Server::READABLE ],
				],
				'/menus/(?P<mid>\d+)' => [
					[ [ $this, 'get_menu_items_by_id' ],		WP_JSON_Server::READABLE ],
				],
				'/menus/all/meta' => [
					[ [ $this, 'get_menus_meta' ], 					WP_JSON_Server::READABLE ],
				],
				'/menus/(?P<name>[a-z-]+)/meta' => [
					[ [ $this, 'get_menu_meta_by_name' ], 	WP_JSON_Server::READABLE ],
				],
				'/menus/(?P<mid>\d+)/meta' => [
					[ [ $this, 'get_menu_meta_by_id' ],			WP_JSON_Server::READABLE ],
				],
			];

			return array_merge($routes, $menus_routes);
		}

		public function get_menus( $_method, $_route, $_path, $_headers ) {
			$json_url = get_json_url() . $_path . '/';
			$menus = wp_get_nav_menus();
			foreach ($menus as $key => $menu) {
				$menus[$key]->items = $this->_get_menu_items($menu->slug, '/menus/items/'.$menu->slug);
				$menus[$key]->meta = [
					'links' => [
						'collection' => $json_url,
						'self' => $json_url . $menu->slug
					]
				];
			}
			return $menus;
		}

		public function get_menu_items_by_name( $name, $_method, $_route, $_path, $_headers ) {
			if( empty($name) || !is_string($name) )
				return new WP_Error( 'json_menu_invalid_name', __( "Invalid menu name" ), ['status' => 404] );
			return $this->_get_menu_items($name, $_path);
		}

		public function get_menu_items_by_id( $mid, $_method, $_route, $_path, $_headers ) {
			if( empty($mid) || !is_numeric($mid) )
				return new WP_Error( 'json_menu_invalid_id', __( "Invalid menu id"), ['status' => 404] );
			return $this->_get_menu_items($mid, $_path);
		}

		private function _get_menu_items($id, $path) {
			$json_url = get_json_url() . $path;
			$url = explode('/', $json_url);
			array_pop($url);
			$json_url = implode('/', $url) . '/';
			$menu_items = wp_get_nav_menu_items($id);
			$menu_items['meta'] = [
				'links' => [
					'self' => $json_url . $id 
				]
			];
			return $menu_items;
		}

		public function get_menus_meta( $_method, $_route, $_path, $_headers ) {
			$json_url = get_json_url() . $_path . '/';
			$menus = wp_get_nav_menus();
			foreach ($menus as $key => $menu) {
				$menus[$key]->meta = [
					'links' => [
						'collection' => $json_url,
						'self' => $json_url . $menu->term_id
					]
				];
			}
			return $menus;
		}

		public function get_menu_meta_by_name( $name, $_method, $_route, $_path, $_headers ) {
			if( empty($name) || !is_string($name) )
				return new WP_Error( 'json_menu_invalid_name', __( "Invalid menu name" ), ['status' => 404] );

			return $this->_get_menu_meta($name, $_path);
		}

		public function get_menu_meta_by_id( $mid, $_method, $_route, $_path, $_headers ) {
			if( empty($mid) || !is_numeric($mid) )
				return new WP_Error( 'json_menu_invalid_id', __( "Invalid menu id"), ['status' => 404] );

			return $this->_get_menu_meta($mid, $_path);
		}

		private function _get_menu_meta($id, $path) {
			$json_url = get_json_url() . $path;
			$url = explode('/', $json_url);
			array_pop($url);
			$json_url = implode('/', $url) . '/';
			// $menu_items = wp_get_nav_menu_items($id);
			$menu_object = wp_get_nav_menu_object($id);
			$menu_object->meta = [
				'links' => [
					'collection' => $json_url,
					'self' => $json_url . $id 
				]
			];
			return $menu_object;
		}

		public function get_sitemap( $_method, $_route, $_path, $_headers ) {
			$json_url = get_json_url() . $_path;
			$post_types = get_post_types(['public' => true]);
			if(isset($post_types['attachment']))
				unset($post_types['attachment']);
			$posts = $this->query_for_sitemap($post_types);

		  // If we have no pages get out quick
		  if ( !$posts )
		      return array( array(), array() );

		  //now reverse it, because we need parents after children for rewrite rules to work properly
		  $posts = array_reverse($posts, true);

		  $page_uris = [];
		  foreach ($posts as $id => $post) {
		  	$uri = get_page_uri($id);
		  	$page_uris[$uri] = $id;
		  }
		  $response = [
		  	'sitemap' => $page_uris,
		  	'meta' => ['links' => ['self' => $json_url ]]
		  ];
		  return $response;
		}

		private function query_for_sitemap($post_types) {
			global $wpdb;
			$query_in = $this->array_to_query_in_string($post_types);
		 
		  //get pages in order of hierarchy, i.e. children after parents
		  $pages = $wpdb->get_results("SELECT ID, post_name, post_parent FROM $wpdb->posts WHERE post_type IN ($query_in) AND post_status = 'publish'");
		  return get_page_hierarchy( $pages );
		}

		/**
		 * Format an array into a string for use in MySQL
		 * WHERE ... IN () clause.
		 *
		 * @param (array) $vals
		 * @return (string) values in single quotes separated by comma
		 */
		private function array_to_query_in_string($vals) {
			$return = '';
		  $i = 1;
		  $count = count($vals);
		  foreach ($vals as $k => $v) {
		    $return .= "'$k'";
		    if($i < $count)
		      $return .= ',';
		    $i++;
		  }
		  return $return;
		}
	}
endif;