<?php 

if( !class_exists('Pixo_JSON_Options') ) :
	class Pixo_JSON_Options {

		public function register_routes( $routes ) {
			$menus_routes = [
				'/acf_options' => [
					[ [ $this, 'get_ACF_options' ], 							WP_JSON_Server::READABLE ],
				],
				'/acf_options/(?P<slug>[a-z-_]+)' => [
					[ [ $this, 'get_ACF_options_by_slug' ],
						WP_JSON_Server::READABLE ],
				],
			];

			return array_merge($routes, $menus_routes);
		}

		public function get_ACF_options( $_method, $_route, $_path, $_headers ) {
			$json_url = get_json_url() . $_path . '/';
			foreach ($GLOBALS['acf_options_pages'] as $page => $settings) {
				$post_id = $settings['post_id'];
				$options[$post_id] = get_fields($post_id);
				$options[$post_id]['meta'] = [
					'links' => [
						'collection' => $json_url,
						'self' => $json_url . $post_id
					]
				];
			}

			return $options;
		}

		public function get_ACF_options_by_slug( $slug, $_method, $_route, $_path, $_headers ) {
			if( empty($slug) || !is_string($slug) )
				return new WP_Error( 'json_options_invalid_slug', __( "Invalid options slug" ), ['status' => 404] );

			$fields = get_fields($slug);
			$json_url = get_json_url() . $_path . '/';
			$fields['meta'] = [
				'links' => [
					'collection' => $json_url,
					'self' => $json_url . $slug
				]
			];

			return $fields;
		}
	}
endif;
