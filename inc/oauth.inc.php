<?php // oauth.inc.php

function pixo_allow_local_oauth_requests($external, $host, $url) {
  if(parse_url($url, PHP_URL_PATH) == '/_callback_auth.php')
    return TRUE;
  return FALSE;
}